package com.hidrantes.cbmmg.hidrantes.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.hidrantes.cbmmg.hidrantes.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import util.Constants;
import util.KeyGenerator;
import util.MaskEditUtil;

public class DeleteHydrantActivity extends AppCompatActivity {

    private int currentHydrantId;
    Response response = null;

    // Required fields flags
    private boolean nReds_correct = false,
            obs_filled = false;

    // Layout input elements
    private ImageView hydrantIcon = null;
    private TextView hydrantId = null;
    private TextView hydrantAddress = null;
    private ImageView redsValid = null;
    private ImageView redsInvalid = null;
    private TextInputLayout nRedsTextInput = null;
    private TextInputEditText nReds = null;
    private TextView errorNReds = null;
    private TextView observations = null;
    private Button btnApagarHidrante = null;

    private String redsValidator = null;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_hydrant);

        Intent intent = getIntent();
        fillCurrentHydrantData(intent);

        redsValid = findViewById(R.id.reds_valid);
        redsInvalid = findViewById(R.id.reds_invalid);
        nReds = findViewById(R.id.input_n_reds);
        nRedsTextInput = findViewById(R.id.text_input_n_reds);
        errorNReds = findViewById(R.id.textinput_error);
        observations = findViewById(R.id.input_observacoes);
        btnApagarHidrante = findViewById(R.id.apagarHidranteBtn);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        btnApagarHidrante.setEnabled(false);
        errorNReds.setPadding(0, 4, 0, 0);
        hideNRedsStatus();

        TextWatcher nRedsLengthValidatorTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (nReds.getText().toString().length() == Constants.N_REDS_FULL_LENGTH) {
                    verifyIfNRedsExists();
                } else {
                    nReds_correct = false;
                    hideNRedsStatus();
                    verifyRequiredFields();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        TextWatcher observationsLengthValidatorTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(observations.getText().toString().length() > 0) {
                    obs_filled = true;
                } else {
                    obs_filled = false;
                }
                verifyRequiredFields();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        nReds.addTextChangedListener(MaskEditUtil.mask(nReds, Constants.N_REDS_MASK_FORMAT));
        nReds.addTextChangedListener(nRedsLengthValidatorTextWatcher);
        nReds.setText( String.valueOf(Calendar.getInstance().get(Calendar.YEAR)) );

        observations.addTextChangedListener(observationsLengthValidatorTextWatcher);
    }

    public void fillCurrentHydrantData(Intent intent) {
        JSONObject hydrant;
        hydrantIcon = findViewById(R.id.hydrant_logo);
        hydrantId = findViewById(R.id.hydrant_id);
        hydrantAddress = findViewById(R.id.hydrant_address);

        try {
            hydrant = new JSONObject(intent.getStringExtra("HYDRANT"));
            hydrantIcon.setImageResource(hydrant.getInt("icon"));
            currentHydrantId = hydrant.getInt("id");
            hydrantId.setText( String.format("%s %d", getString(R.string.hydrant), hydrant.getInt("id")) );
            hydrantAddress.setText(hydrant.getString("address"));
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Erro ao carregar hidrante.", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public void verifyRequiredFields() {
        if(nReds_correct && obs_filled) {
            btnApagarHidrante.setEnabled(true);
        } else {
            btnApagarHidrante.setEnabled(false);
        }
    }

    public void confirmDeleteHydrant(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(DeleteHydrantActivity.this, android.R.style.Theme_Material_Light_Dialog);

        builder.setMessage(R.string.apagar_hidrante_confirmacao_dialog_msg)
                .setTitle(R.string.apagar_hidrante_confirmacao_dialog_title);
        builder.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteHydrant();
            }
        });
        builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void deleteHydrant() {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Apagando hidrante", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + Constants.DELETE_HYDRANT_ROUTE;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, createHydrantJson().toString());
                builder.post(body);
                Request request = builder.build();

                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(DeleteHydrantActivity.this, android.R.style.Theme_Material_Light_Dialog);
                            builder.setMessage(R.string.apagar_hidrante_sucesso_dialog_msg)
                                    .setTitle(R.string.apagar_hidrante_sucesso_dialog_title);
                            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent it = new Intent(DeleteHydrantActivity.this, MainActivity.class);
                                    startActivity(it);
                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            Toast.makeText(DeleteHydrantActivity.this,
                                    "Ocorreu um erro ao realizar a vistoria. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                        }
                    }
                });
            }
        }).start();
    }

    public void goBack(View v) {
        DeleteHydrantActivity.this.finish();
    }

    public JSONObject createHydrantJson() {

        JSONObject currentHydrant = new JSONObject();

        try {
            currentHydrant.put("militaryNumber", preferences.getString("nbm", "missing"));
            currentHydrant.put("hydrantId", currentHydrantId);
            currentHydrant.put("nReds", nReds.getText().toString());
            currentHydrant.put("observations", observations.getText().toString());
        } catch (JSONException e) {
            return null;
        }

        return currentHydrant;
    }

    public void hideNRedsStatus() {
        redsValid.setVisibility(View.GONE);
        redsInvalid.setVisibility(View.GONE);
        nRedsTextInput.setError(null);
    }

    public void verifyIfNRedsExists() {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Verificando Nº do REDS", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + Constants.REDS_VERIFIER_ROUTE;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, createNRedsJson().toString());
                builder.post(body);
                Request request = builder.build();

                try {
                    response = client.newCall(request).execute();

                    if (response.isSuccessful())
                        redsValidator = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        if (redsValidator != null)
                            validateRedsNumber(redsValidator);
                    }
                });
            }
        }).start();
    }

    public JSONObject createNRedsJson() {
        JSONObject currentNReds = new JSONObject();

        try {
            currentNReds.put("militaryNumber", preferences.getString("nbm", "missing"));
            currentNReds.put("nReds", nReds.getText().toString());
        } catch (JSONException e) {
            return null;
        }

        return currentNReds;
    }

    public void validateRedsNumber(String redsValidator) {
        try {
            JSONObject jsonResponse = new JSONObject(redsValidator);

            if (!jsonResponse.getBoolean("exists")) {
                nReds_correct = true;
                redsValid.setVisibility(View.VISIBLE);
                redsInvalid.setVisibility(View.GONE);
                nRedsTextInput.setError(null);
            } else {
                nReds_correct = false;
                redsValid.setVisibility(View.GONE);
                redsInvalid.setVisibility(View.VISIBLE);
                nRedsTextInput.setError("Nº REDS já utilizado");
            }

            verifyRequiredFields();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
