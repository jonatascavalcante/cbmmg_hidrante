package com.hidrantes.cbmmg.hidrantes.models;

public class User {

    private String nBm;
    private String name;

    public User(String nBm, String name) {
        this.nBm = nBm;
        this.name = name;
    }

    public String getnBm() {
        return nBm;
    }

    public void setnBm(String nBm) {
        this.nBm = nBm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "nBm='" + nBm + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
