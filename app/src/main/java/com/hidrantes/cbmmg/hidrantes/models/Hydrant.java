package com.hidrantes.cbmmg.hidrantes.models;

import com.google.android.gms.maps.model.LatLng;
import com.hidrantes.cbmmg.hidrantes.R;

import org.json.JSONException;
import org.json.JSONObject;

public class Hydrant {
    private int id;
    private String icon;
    private String unit;
    private String address;
    private String referencePoint;
    private LatLng position;
    private String situation;
    private String output;
    private String pressure;
    private String cap;
    private Inspection lastInspection;

    public Hydrant(int id, String icon, String unit, String address, String referencePoint, LatLng position,
                   String situation, String output, String pressure, String cap,
                   Inspection lastInspection) {
        this.id = id;
        this.icon = icon;
        this.unit = unit;
        this.address = address;
        this.referencePoint = referencePoint;
        this.position = position;
        this.situation = situation;
        this.output = output;
        this.pressure = pressure;
        this.cap = cap;
        this.lastInspection = lastInspection;
    }

    public Hydrant(String icon, String address, LatLng position) {
        this.icon = icon;
        this.unit = "";
        this.address = address;
        this.referencePoint = "";
        this.position = position;
        this.situation = "";
        this.output = "";
        this.pressure = "";
        this.cap = "";
        this.lastInspection = new Inspection();
    }

    public Hydrant(String icon, int id, double latitude, double longitude) {
        this.id = id;
        this.icon = icon;
        this.position = new LatLng(latitude, longitude);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        int iconPicture = R.drawable.red_hydrant;

        switch(icon) {
            case "red_hydrant":
                break;
            case "yellow_hydrant":
                iconPicture = R.drawable.yellow_hydrant;
                break;
            case "black_hydrant":
                iconPicture = R.drawable.dark_hydrant;
                break;
            case "block_hydrant":
                iconPicture = R.drawable.forbidden_hydrant;
                break;
        }
        return iconPicture;
    }

    public String getIconPicture() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReferencePoint() {
        return referencePoint;
    }

    public void setReferencePoint(String referece_point) {
        this.referencePoint = referece_point;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public Inspection getLastInspection() {
        return lastInspection;
    }

    public void setLastInspection(Inspection lastInspection) {
        this.lastInspection = lastInspection;
    }

    @Override
    public String toString() {
        return "Hydrant{" +
                "id=" + id +
                ", icon='" + icon + '\'' +
                ", unit='" + unit + '\'' +
                ", address='" + address + '\'' +
                ", referencePoint='" + referencePoint + '\'' +
                ", position=" + position +
                ", situation='" + situation + '\'' +
                ", output='" + output + '\'' +
                ", pressure='" + pressure + '\'' +
                ", cap='" + cap + '\'' +
                ", lastInspection=" + lastInspection.toString() +
                '}';
    }

    public JSONObject toJson() {
        JSONObject jsonHydrant = new JSONObject();
        try {
            jsonHydrant.put("id", id);
            jsonHydrant.put("icon", getIcon());
            jsonHydrant.put("unit", unit);
            jsonHydrant.put("address", address);
            jsonHydrant.put("referencePoint", referencePoint);
            jsonHydrant.put("position", position);
            jsonHydrant.put("situation", situation);
            jsonHydrant.put("output", output);
            jsonHydrant.put("pressure", pressure);
            jsonHydrant.put("cap", cap);
            jsonHydrant.put("lastInspectionDate", lastInspection.getInspectionDate());
            jsonHydrant.put("lastInspectionReds", lastInspection.getRedsNumber());
            jsonHydrant.put("lastInspectionObs", lastInspection.getObservations());
        } catch (JSONException e) {
            return null;
        }

        return jsonHydrant;
    }
}
