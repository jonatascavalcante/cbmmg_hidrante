package com.hidrantes.cbmmg.hidrantes.models;
import java.util.Calendar;

public class Inspection {

    private String inspectionDate;
    private String redsNumber;
    private String observations;

    public Inspection(String inspectionDate, String redsNumber, String observations) {
        this.inspectionDate = inspectionDate;
        this.redsNumber = redsNumber;
        this.observations = observations;
    }

    public Inspection() {
        this.inspectionDate = "";
        this.redsNumber = "";
        this.observations = "";
    }

    public String getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getRedsNumber() {
        return this.redsNumber;
    }

    public void setRedsNumber(String redsNumber) {
        this.redsNumber = redsNumber;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    @Override
    public String toString() {
        return "Inspection{" +
                "inspectionDate='" + inspectionDate + '\'' +
                ", redsNumber='" + redsNumber + '\'' +
                ", observations='" + observations + '\'' +
                '}';
    }
}
