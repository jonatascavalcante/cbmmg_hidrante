package com.hidrantes.cbmmg.hidrantes.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.hidrantes.cbmmg.hidrantes.R;

public class SearchHydrantActivity extends AppCompatActivity {

    private EditText hydrantId = null;
    private Button btnPesquisar = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hydrant_search);

        hydrantId = findViewById(R.id.hydrant_id_input);
        btnPesquisar = findViewById(R.id.filtrar);

        btnPesquisar.setEnabled(false);

        TextWatcher hydrantLengthValidatorTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(hydrantId.getText().toString().length() > 0) {
                    btnPesquisar.setEnabled(true);
                } else {
                    btnPesquisar.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        hydrantId.addTextChangedListener(hydrantLengthValidatorTextWatcher);
    }

    public void goBack(View v)
    {
        SearchHydrantActivity.this.finish();
    }

    public void search(View v)
    {
        Intent intent = new Intent(SearchHydrantActivity.this, MainActivity.class);
        intent.putExtra("SEARCH", Integer.parseInt(hydrantId.getText().toString()));
        startActivity(intent);
    }
}
