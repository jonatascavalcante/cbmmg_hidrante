package com.hidrantes.cbmmg.hidrantes.ui;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.hidrantes.cbmmg.hidrantes.R;

import com.hidrantes.cbmmg.hidrantes.models.Hydrant;
import com.hidrantes.cbmmg.hidrantes.models.Unit;

import java.text.DecimalFormat;
import java.util.ArrayList;

import util.Constants;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private View bottomSheet;
    private int bottomSheetPeekHeight;
    private Hydrant currentHydrant;
    private ArrayList<Unit> allUnits;
    private String userLat, userLong;
    private boolean isEditable;

    public BottomSheetFragment(Hydrant hydrant, ArrayList<Unit> units, double lat, double lng, boolean isEditable) {
        currentHydrant = hydrant;
        allUnits = units;
        userLat = Double.toString(lat).replace(',', '.');
        userLong = Double.toString(lng).replace(',', '.');
        this.isEditable = isEditable;
    }

    @Override public int getTheme() {
        return R.style.Theme_MaterialComponents_Light_BottomSheetDialog;
    }

    @Nullable
    @Override public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        final View view = inflater
                .inflate(R.layout.bottom_sheet_hydrant_info, container, false);

        bottomSheet = view.findViewById(R.id.hydrant_id);

        ImageView hydrantIcon = view.findViewById(R.id.hydrant_logo);
        FloatingActionButton navigationButton = view.findViewById(R.id.navigation);
        hydrantIcon.setImageResource(currentHydrant.getIcon());
        TextView hydrantId = view.findViewById(R.id.hydrant_id);
        TextView hydrantAddress = view.findViewById(R.id.hydrant_address);
        TextView hydrantReferencePoint = view.findViewById(R.id.hydrant_reference_point);
        TextView hydrantLastInspectionDate = view.findViewById(R.id.hydrant_last_inspection_date);
        TextView hydrantLastInspectionNReds = view.findViewById(R.id.hydrant_last_inspection_n_reds);
        TextView hydrantSituation = view.findViewById(R.id.hydrant_status);
        TextView hydrantOutput = view.findViewById(R.id.hydrant_output);
        TextView hydrantPressure = view.findViewById(R.id.hydrant_pressure);
        TextView hydrantCap = view.findViewById(R.id.hydrant_cap);
        TextView hydrantObs = view.findViewById(R.id.hydrant_obs);
        TextView hydrantLatLng = view.findViewById(R.id.hydrant_lat_lng);
        Button editarLocalizacaoBtn = view.findViewById(R.id.editarLocalizacaoBtn);
        TextView hydrantUnit = view.findViewById(R.id.hydrant_unit);
        Button fazerVistoriaBtn = view.findViewById(R.id.fazerVistoriaBtn);
        Button apagarHidranteBtn = view.findViewById(R.id.apagarHidranteBtn);

        String latitude = convertCoordinateToDMS(currentHydrant.getPosition().latitude);
        String longitude = convertCoordinateToDMS(currentHydrant.getPosition().longitude);

        hydrantId.setText( String.format("%s %d", getString(R.string.hydrant), currentHydrant.getId()) );
        hydrantAddress.setText(currentHydrant.getAddress());
        hydrantReferencePoint.setText(currentHydrant.getReferencePoint());
        hydrantLastInspectionDate.setText(currentHydrant.getLastInspection().getInspectionDate());
        hydrantLastInspectionNReds.setText(currentHydrant.getLastInspection().getRedsNumber());
        hydrantSituation.setText(currentHydrant.getSituation());
        hydrantOutput.setText(currentHydrant.getOutput());
        hydrantPressure.setText(currentHydrant.getPressure());
        hydrantCap.setText(currentHydrant.getCap());
        hydrantObs.setText(currentHydrant.getLastInspection().getObservations());
        hydrantLatLng.setText(latitude + "S, " + longitude + "W");
        hydrantUnit.setText(currentHydrant.getUnit());

        bottomSheetPeekHeight = getResources()
                .getDimensionPixelSize(R.dimen.bottom_sheet_default_peek_height);

        if (!isEditable) {
            editarLocalizacaoBtn.setVisibility(View.GONE);
        }

        fazerVistoriaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), InspectionActivity.class);
                intent.putExtra("HYDRANT", currentHydrant.toJson().toString());
                intent.putExtra("UNITS", new Gson().toJson(allUnits));
                startActivity(intent);
            }
        });

        apagarHidranteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DeleteHydrantActivity.class);
                intent.putExtra("HYDRANT", currentHydrant.toJson().toString());
                startActivity(intent);
            }
        });

        navigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lat = Double.toString(currentHydrant.getPosition().latitude).replace(',', '.');
                String lng = Double.toString(currentHydrant.getPosition().longitude).replace(',', '.');

                String addr = String.format("%s%s, %s%s%s, %s", Constants.GOOGLE_MAPS_URL_START, userLat, userLong,
                        Constants.GOOGLE_MAPS_URL_MIDDLE, lat, lng);

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(addr));
                startActivity(intent);
            }
        });

        editarLocalizacaoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddHydrantActivity.class);
                intent.putExtra("LAT", currentHydrant.getPosition().latitude);
                intent.putExtra("LNG", currentHydrant.getPosition().longitude);
                intent.putExtra("UNITS", new Gson().toJson(allUnits));
                intent.putExtra("HYDRANT_ID", currentHydrant.getId());
                intent.putExtra("EDIT", "true");
                startActivity(intent);
            }
        });

        return view;
    }

    @Override public void onResume() {
        super.onResume();
        setUpBottomSheet();
    }

    private void setUpBottomSheet() {
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior
                .from((View) getView().getParent());

        bottomSheetBehavior.setPeekHeight(bottomSheetPeekHeight);

        final ViewGroup.LayoutParams childLayoutParams = bottomSheet.getLayoutParams();
        final DisplayMetrics displayMetrics = new DisplayMetrics();

        requireActivity()
                .getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        childLayoutParams.height = displayMetrics.heightPixels;

        bottomSheet.setLayoutParams(childLayoutParams);
    }

    private String convertCoordinateToDMS(double coordinate) {
        coordinate *= -1;
        int degrees = (int) coordinate;
        double minutes = (coordinate - degrees) * 60;
        double seconds = (minutes - (int) minutes) * 60;

        return degrees + "º " + (int) minutes + "\' " +
                new DecimalFormat("##.##").format(seconds) + "\" ";
    }
}
