package com.hidrantes.cbmmg.hidrantes.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.hidrantes.cbmmg.hidrantes.R;
import com.hidrantes.cbmmg.hidrantes.models.Unit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import util.Constants;
import util.KeyGenerator;
import util.MaskEditUtil;

import util.PDFGenerator;

import static util.Constants.PERMISSIONS_REQUEST_ACCESS_WRITE_EXTERNAL_STORAGE;

public class FilterHydrantActivity extends AppCompatActivity {

    private Spinner units = null;
    private EditText dateFrom = null, dateTo = null;
    private boolean ck_red_hydrant_checked = false,
            ck_yellow_hydrant_checked = false,
            ck_black_hydrant_checked = false,
            ck_block_hydrant_checked = false;

    SharedPreferences preferences;
    Response response = null;
    private String hydrants = null;
    private boolean mWritePermissionGranted = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_hydrant_filter);

        Intent intent = getIntent();
        fillUnitsSpinner(intent);

        CheckBox redHydrant = findViewById(R.id.red_hydrant);
        CheckBox yellowHydrant = findViewById(R.id.yellow_hydrant);
        CheckBox blackHydrant = findViewById(R.id.black_hydrant);
        CheckBox blockHydrant = findViewById(R.id.block_hydrant);

        dateFrom = findViewById(R.id.dateFrom);
        dateTo = findViewById(R.id.dateTo);

        redHydrant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ck_red_hydrant_checked = buttonView.isChecked();
            }
        });

        yellowHydrant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ck_yellow_hydrant_checked = buttonView.isChecked();
            }
        });

        blackHydrant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ck_black_hydrant_checked = buttonView.isChecked();
            }
        });

        blockHydrant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ck_block_hydrant_checked = buttonView.isChecked();
            }
        });

        dateFrom.addTextChangedListener(MaskEditUtil.mask(dateFrom, Constants.DATE_SIMPLE_FORMAT));
        dateTo.addTextChangedListener(MaskEditUtil.mask(dateTo, Constants.DATE_SIMPLE_FORMAT));
    }

    public void goBack(View v) {
        FilterHydrantActivity.this.finish();
    }

    public void fillUnitsSpinner(Intent intent)
    {
        JSONArray allUnitsJson;
        ArrayList<Unit> allUnits;

        try {
            allUnitsJson = new JSONArray(intent.getStringExtra("UNITS"));
            allUnits = InspectionActivity.getAllUnitsFromJson(allUnitsJson);
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Erro ao carregar as unidades.", Toast.LENGTH_SHORT).show();
            return;
        }

        List<String> spinnerArray = new ArrayList<>();

        spinnerArray.add(Constants.ALL_UNITS);

        for (int i = 0; i < allUnits.size(); i++) {
            spinnerArray.add(allUnits.get(i).getDescription());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = findViewById(R.id.units_spinner);
        sItems.setAdapter(adapter);

        int spinnerPosition = adapter.getPosition(Constants.DEFAULT_UNIT);
        sItems.setSelection(spinnerPosition);
    }


    public void filter(View v)
    {
        Intent intent = new Intent(FilterHydrantActivity.this, MainActivity.class);
        intent.putExtra("FILTERS", createFiltersJson(false).toString());
        startActivity(intent);
    }

    public JSONObject createFiltersJson(boolean isPDF)
    {
        JSONObject filters = new JSONObject();

        try {
            if (unitWasSelected())
                filters.put("unit", units.getSelectedItem().toString());

            if (isPDF)
                filters = validatePDFSelectedStatus(filters);
            else
                filters = validateSelectedStatus(filters);

            if (!dateFrom.getText().toString().isEmpty())
                filters.put("dateFrom", dateFrom.getText().toString());

            if (!dateTo.getText().toString().isEmpty())
                filters.put("dateTo", dateTo.getText().toString());

            } catch (JSONException e) {
                return null;
            }

        return filters;
    }

    public boolean unitWasSelected()
    {
        units = findViewById(R.id.units_spinner);

        return !units.getSelectedItem().toString().equals(Constants.ALL_UNITS);
    }

    public JSONObject validateSelectedStatus(JSONObject filters)
    {
        ArrayList<String> status = new ArrayList<>();

        if (ck_red_hydrant_checked) status.add("red_hydrant");
        if (ck_yellow_hydrant_checked) status.add("yellow_hydrant");
        if (ck_black_hydrant_checked) status.add("black_hydrant");
        if (ck_block_hydrant_checked) status.add("block_hydrant");

        try {
            filters.put("status", new Gson().toJson(status));
        } catch (JSONException e) {
            return null;
        }

        return filters;
    }

    public JSONObject validatePDFSelectedStatus(JSONObject filters)
    {
        String status;

        if (ck_red_hydrant_checked && ck_yellow_hydrant_checked && ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Todos hidrantes";
        else if (!ck_red_hydrant_checked && !ck_yellow_hydrant_checked && !ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Hidrantes inoperantes";
        else if (!ck_red_hydrant_checked && !ck_yellow_hydrant_checked && ck_black_hydrant_checked && !ck_block_hydrant_checked)
            status = "Hidrantes não vistoriados há mais de um ano";
        else if (!ck_red_hydrant_checked && !ck_yellow_hydrant_checked && ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Hidrantes não vistoriados há mais de um ano ou inoperantes";
        else if (!ck_red_hydrant_checked && ck_yellow_hydrant_checked && !ck_black_hydrant_checked && !ck_block_hydrant_checked)
            status = "Hidrantes vistoriados entre os últimos 7 meses a um ano";
        else if (!ck_red_hydrant_checked && ck_yellow_hydrant_checked && !ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Hidrantes vistoriados entre os últimos 7 meses a um ano ou inoperantes";
        else if (!ck_red_hydrant_checked && ck_yellow_hydrant_checked && ck_black_hydrant_checked && !ck_block_hydrant_checked)
            status = "Hidrantes vistoriados entre os últimos 7 meses a 1 ano/não vistoriados há mais de 1 ano";
        else if (!ck_red_hydrant_checked && ck_yellow_hydrant_checked && ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Hidrantes não vistoriados nos últimos 6 meses ou inoperantes";
        else if (ck_red_hydrant_checked && !ck_yellow_hydrant_checked && !ck_black_hydrant_checked && !ck_block_hydrant_checked)
            status = "Hidrantes vistoriados nos últimos 6 meses";
        else if (ck_red_hydrant_checked && !ck_yellow_hydrant_checked && !ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Hidrantes vistoriados nos últimos 6 meses ou inoperantes";
        else if (ck_red_hydrant_checked && !ck_yellow_hydrant_checked && ck_black_hydrant_checked && !ck_block_hydrant_checked)
            status = "Hidrantes vistoriados nos últimos 6 meses ou não vistoriados há mais de um ano";
        else if (ck_red_hydrant_checked && !ck_yellow_hydrant_checked && ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Hidrantes vistoriados nos últimos 6 meses/não vistoriados há mais de 1 ano/inoperantes";
        else if (ck_red_hydrant_checked && ck_yellow_hydrant_checked && !ck_black_hydrant_checked && !ck_block_hydrant_checked)
            status = "Hidrantes vistoriados no último ano";
        else if (ck_red_hydrant_checked && ck_yellow_hydrant_checked && !ck_black_hydrant_checked && ck_block_hydrant_checked)
            status = "Hidrantes vistoriados no último ano ou inoperantes";
        else if (ck_red_hydrant_checked && ck_yellow_hydrant_checked && ck_black_hydrant_checked && !ck_block_hydrant_checked)
            status = "Hidrantes operantes";
        else
            status = "Nenhum hidrante";

        try {
            filters.put("status", status);
        } catch (JSONException e) {
            return null;
        }

        return filters;
    }

    public void export(View v) {
        if (!mWritePermissionGranted)
            checkPermissionToWrite();
        else
            exportHydrants();
    }

    public void exportHydrants() {
        try {
            JSONObject filters = new JSONObject(createFiltersJson(false).toString());
            filters.put("militaryNumber", preferences.getString("nbm", "missing"));
            getFilteredHydrants(filters, Constants.EXPORTED_HYDRANTS_ROUTE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getFilteredHydrants(final JSONObject requestBody, final String urlRoute)
    {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Obtendo dados dos hidrantes", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + urlRoute;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, requestBody.toString());
                builder.post(body);
                Request request = builder.build();

                try {
                    response = client.newCall(request).execute();
                    if (response == null) {
                        Toast.makeText(FilterHydrantActivity.this,
                                "Servidor está fora do ar. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                    } else {
                        if (response.isSuccessful()) {
                            hydrants = response.body().string();
                        } else {
                            hydrants = new String("error");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (hydrants != null) {
                            if (hydrants.contains("error")) {
                                Toast.makeText(FilterHydrantActivity.this,
                                        "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                            } else {
                                progressDialog.dismiss();
                                openPDF(hydrants);
                            }
                        } else {
                            Toast.makeText(FilterHydrantActivity.this,
                                    "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    private File createPDF(String hydrants)
    {
        PDFGenerator pdfGenerator = new PDFGenerator(this);
        PdfDocument document = pdfGenerator.createPDF(hydrants, createFiltersJson(true));
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "report.pdf");

        if (mWritePermissionGranted) {
            try {
                document.writeTo(new FileOutputStream(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        document.close();
        return file;
    }

    private void openPDF(final String hydrants)
    {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Gerando PDF", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                final Uri fileURI = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", createPDF(hydrants));

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(fileURI, "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(intent);
                    }
                });
            }
        }).start();
    }

    private void checkPermissionToWrite()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_ACCESS_WRITE_EXTERNAL_STORAGE);
        } else {
            mWritePermissionGranted = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mWritePermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mWritePermissionGranted = true;
                    exportHydrants();
                }
            }
        }
    }
}


