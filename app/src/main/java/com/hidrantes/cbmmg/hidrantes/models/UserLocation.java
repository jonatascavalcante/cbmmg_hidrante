package com.hidrantes.cbmmg.hidrantes.models;

import com.google.android.gms.maps.model.LatLng;
import java.util.Date;

public class UserLocation {

    private User user;
    private LatLng location;
    private Date timestamp;

    public UserLocation(User user, LatLng location, Date timestamp) {
        this.user = user;
        this.location = location;
        this.timestamp = timestamp;
    }

    public UserLocation() {

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "UserLocation{" +
                "user=" + user +
                ", location =" + location +
                ", timestamp=" + timestamp +
                '}';
    }
}
