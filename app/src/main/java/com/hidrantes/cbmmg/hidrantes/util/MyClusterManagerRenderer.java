package com.hidrantes.cbmmg.hidrantes.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.hidrantes.cbmmg.hidrantes.R;
import com.hidrantes.cbmmg.hidrantes.models.ClusterMarker;

public class MyClusterManagerRenderer extends DefaultClusterRenderer<ClusterMarker> {

    private final IconGenerator mIconGenerator;
    private final IconGenerator mClusterIconGenerator;
    private final ImageView mImageView;
    private final ImageView mClusterImageView;
    private final int markerWidth;
    private final int markerHeight;
    private final int clusterWidth;
    private final int clusterHeight;
    private Context context;

    public MyClusterManagerRenderer(Context context, GoogleMap map, ClusterManager<ClusterMarker> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;

        mClusterIconGenerator = new IconGenerator(context.getApplicationContext());
        mClusterImageView = new ImageView(context.getApplicationContext());
        mIconGenerator = new IconGenerator(context.getApplicationContext());
        mImageView = new ImageView(context.getApplicationContext());

        markerWidth = (int) context.getResources().getDimension(R.dimen.custom_marker_image);
        markerHeight = (int) context.getResources().getDimension(R.dimen.custom_marker_image);
        clusterWidth = (int) context.getResources().getDimension(R.dimen.cluster_marker_image);
        clusterHeight = (int) context.getResources().getDimension(R.dimen.cluster_marker_image);

        mImageView.setLayoutParams(new ViewGroup.LayoutParams(markerWidth, markerHeight));
        mClusterImageView.setLayoutParams(new ViewGroup.LayoutParams(clusterWidth, clusterHeight));

        int padding = (int) context.getResources().getDimension(R.dimen.custom_marker_padding);
        mImageView.setPadding(padding, padding, padding, padding);
        mIconGenerator.setContentView(mImageView);
        mClusterIconGenerator.setContentView(mClusterImageView);
    }

    @Override
    protected void onBeforeClusterItemRendered(ClusterMarker item, MarkerOptions markerOptions) {
        int iconPicture = R.drawable.red_hydrant;

        switch(item.getIconPicture()) {
            case "red_hydrant":
                break;
            case "yellow_hydrant":
                iconPicture = R.drawable.yellow_hydrant;
                break;
            case "black_hydrant":
                iconPicture = R.drawable.dark_hydrant;
                break;
            case "block_hydrant":
                iconPicture = R.drawable.forbidden_hydrant;
                break;
        }
        mImageView.setImageResource(iconPicture);
        Bitmap icon = mIconGenerator.makeIcon();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(item.getTitle());
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<ClusterMarker> cluster, MarkerOptions markerOptions) {
        int width = clusterWidth;
        int height = clusterHeight;
        int iconPicture = R.drawable.red_hydrant;

        if(cluster.getSize() < 20) {
            iconPicture = R.drawable.ten_hydrant;
        } else if(cluster.getSize() < 50) {
            iconPicture = R.drawable.twenty_hydrant;
        } else if(cluster.getSize() < 100) {
            iconPicture = R.drawable.fifty_hydrant;
        } else if(cluster.getSize() < 200) {
            iconPicture = R.drawable.one_hundred_hydrant;
        } else if(cluster.getSize() < 500) {
            iconPicture = R.drawable.two_hundred_hydrant;
        } else if(cluster.getSize() < 1000) {
            iconPicture = R.drawable.five_hundred_hydrant;
        } else if(cluster.getSize() >= 1000){
            iconPicture = R.drawable.one_thousand_hydrant;
        }

        Drawable drawable = context.getResources().getDrawable(iconPicture);
        drawable.setBounds(0, 0, width, height);
        mClusterImageView.setImageDrawable(drawable);
        Bitmap icon = mClusterIconGenerator.makeIcon();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<ClusterMarker> cluster) {
        return cluster.getSize() > 10;
    }

}
