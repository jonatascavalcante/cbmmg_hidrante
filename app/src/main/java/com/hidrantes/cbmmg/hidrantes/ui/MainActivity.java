package com.hidrantes.cbmmg.hidrantes.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.maps.android.clustering.ClusterManager;
import com.hidrantes.cbmmg.hidrantes.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.hidrantes.cbmmg.hidrantes.models.*;
import com.hidrantes.cbmmg.hidrantes.util.MyClusterManagerRenderer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import util.Constants;
import util.KeyGenerator;

import static util.Constants.ERROR_DIALOG_REQUEST;
import static util.Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;
import static util.Constants.PERMISSIONS_REQUEST_ENABLE_GPS;
import static util.Constants.MAPVIEW_BUNDLE_KEY;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private boolean mLocationPermissionGranted = false;
    private FusedLocationProviderClient mFusedLocationClient;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private LatLngBounds mMapBoundary;
    private UserLocation mUserPosition;
    private ClusterManager mClusterManager;
    private MyClusterManagerRenderer mClusterManagerRenderer;
    private ArrayList<ClusterMarker> mClusterMarkers = new ArrayList<>();
    Response response = null;
    private String hydrants = null;
    private String hydrant;
    private Hydrant currentHydrant = null, searchedHydrant = null;
    private String requestUnits;
    private ArrayList<Unit> units = null;
    private FloatingActionButton settings = null;
    private FloatingActionButton plus = null;
    private FloatingActionButton filter = null;
    private FloatingActionButton search = null;
    private FloatingActionButton report = null;
    private FloatingActionButton feedback = null;
    private FloatingActionButton exit = null;
    private boolean isFABOpen = false;
    private int searchHydrantId = 0;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mMapView = findViewById(R.id.map);
        mMapView.onCreate(mapViewBundle);

        settings = findViewById(R.id.settings);
        plus = findViewById(R.id.plus);
        filter = findViewById(R.id.filter);
        search = findViewById(R.id.search);
        report = findViewById(R.id.report);
        feedback = findViewById(R.id.feedback);
        exit = findViewById(R.id.exit);

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFABOpen){
                    showFABMenu();
                } else{
                    closeFABMenu();
                }
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddHydrantActivity.class);
                intent.putExtra("LAT", mUserPosition.getLocation().latitude);
                intent.putExtra("LNG", mUserPosition.getLocation().longitude);
                intent.putExtra("UNITS", new Gson().toJson(units));
                intent.putExtra("EDIT", "false");
                closeFABMenu();
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchHydrantActivity.class);
                startActivity(intent);
            }
        });

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FilterHydrantActivity.class);
                intent.putExtra("UNITS", new Gson().toJson(units));
                startActivity(intent);
            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AppInfoActivity.class);
                startActivity(intent);
            }
        });

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
                        Uri.fromParts("mailto", "nts.web@bombeiros.mg.gov.br", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feeback do aplicativo \"Bombeiros MG - Hidrantes\"");
                startActivityForResult(emailIntent, 1);
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                preferences.edit().putBoolean("logged", false).apply();
                startActivity(intent);
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mUserPosition = new UserLocation();
    }

    private void addMapsMarkers(String hydrants) {

        if (mGoogleMap != null) {

            if (mClusterManager == null) {
                mClusterManager = new ClusterManager<ClusterMarker>(getApplicationContext(), mGoogleMap);
            }
            if (mClusterManagerRenderer == null) {
                mClusterManagerRenderer = new MyClusterManagerRenderer(
                        this,
                        mGoogleMap,
                        mClusterManager
                );
                mClusterManagerRenderer.setAnimation(true);
                mClusterManager.setRenderer(mClusterManagerRenderer);
            }

            try {
                JSONObject hydrantList = new JSONObject(hydrants);

                getLatLongFromSearchedHydrant(hydrantList);

                JSONArray hydrantsArray = hydrantList.getJSONArray("json");

                JSONObject hydrant;

                for (int i = 0; i < hydrantsArray.length(); i++) {
                    hydrant = new JSONObject(hydrantsArray.getString(i));
                    ClusterMarker clusterMarker;

                    clusterMarker = new ClusterMarker(
                            new LatLng(hydrant.getDouble("lat"), hydrant.getDouble("long")),
                            hydrant.getString("id"),
                            "",
                            hydrant.getString("icon"),
                            hydrant.getInt("id")
                    );

                    mClusterManager.addItem(clusterMarker);
                    mClusterMarkers.add(clusterMarker);
                }
            } catch (JSONException e) {
               e.printStackTrace();
            }

            mClusterManager.cluster();

            mGoogleMap.setOnMarkerClickListener(mClusterManager);
            mGoogleMap.setOnCameraIdleListener(mClusterManager);

            mClusterManager.setOnClusterItemClickListener(
                new ClusterManager.OnClusterItemClickListener<ClusterMarker>() {
                    @Override
                    public boolean onClusterItemClick(ClusterMarker clusterItem) {
                        getHydrantInfo(clusterItem.getHydrantId(), clusterItem.getIconPicture());
                        return true;
                    }
                });

            if (searchHydrantId != 0) {
                centerMapOnSearchedHydrant();
            }
        }
    }

    private void setCameraView(double latitude, double longitude) {

        // Set a boundary to start
        double bottomBoundary = latitude - .005;
        double leftBoundary = longitude - .005;
        double topBoundary = latitude + .005;
        double rightBoundary = longitude + .005;

        mMapBoundary = new LatLngBounds(
                new LatLng(bottomBoundary, leftBoundary),
                new LatLng(topBoundary, rightBoundary)
        );

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(mMapBoundary, 0));
    }

    private void plotHydrantsPointsOnMap() {
        if (hydrants == null) {
            try {
                JSONObject filters = new JSONObject(getIntent().getStringExtra("FILTERS"));
                filters.put("militaryNumber", preferences.getString("nbm", "missing"));
                getHydrantsPoints(filters, Constants.FILTERED_HYDRANTS_ROUTE);
            } catch (JSONException | NullPointerException e) {
                searchHydrantId = getIntent().getIntExtra("SEARCH", 0);

                if (searchHydrantId != 0) {
                    getHydrantsPoints(createSearchJson(searchHydrantId), Constants.SEARCHED_HYDRANTS_ROUTE);
                } else {
                    getHydrantsPoints(createLocationJson(), Constants.DELIMITED_HYDRANTS_ROUTE);
                }
            }
        }
    }

    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<android.location.Location>() {
            @Override
            public void onComplete(@NonNull Task<android.location.Location> task) {
                if (task.isSuccessful()) {
                    Location location = task.getResult();
                    mUserPosition.setLocation(new LatLng(location.getLatitude(), location.getLongitude()));
                    if (mUserPosition.getLocation() != null) {
                        setCameraView(mUserPosition.getLocation().latitude, mUserPosition.getLocation().longitude);
                        plotHydrantsPointsOnMap();
                    }

                }
            }
        });
    }

    private boolean checkMapServices() {
        if (isServicesOK()) {
            if (isMapsEnabled()) {
                return true;
            }
        }
        return false;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Essa aplicação requer GPS para funcionar corretamente, deseja habilitá-lo?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isMapsEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            getLastKnownLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    public boolean isServicesOK() {
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MainActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "Você não pode fazer requisições de mapas", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ENABLE_GPS: {
                if (mLocationPermissionGranted) {
                    getLastKnownLocation();
                } else {
                    getLocationPermission();
                }
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mMapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkMapServices()) {
            if (mLocationPermissionGranted) {
                getLastKnownLocation();
            } else {
                getLocationPermission();
            }
        }
        mMapView.getMapAsync(this);
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void getHydrantsPoints(final JSONObject requestBody, final String urlRoute) {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Carregando hidrantes", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + urlRoute;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, requestBody.toString());
                builder.post(body);
                Request request = builder.build();

                if (units == null)
                    getAllUnits();
                try {
                    response = client.newCall(request).execute();
                    if (response == null) {
                        Toast.makeText(MainActivity.this,
                                "Servidor está fora do ar. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                    } else {
                        if (response.isSuccessful()) {
                            hydrants = response.body().string();
                        } else {
                            hydrants = new String("error");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (hydrants != null) {
                            if (hydrants.contains("error")) {
                                Toast.makeText(MainActivity.this,
                                        "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                            } else {
                                progressDialog.dismiss();
                                addMapsMarkers(hydrants);
                            }
                        } else {
                            Toast.makeText(MainActivity.this,
                                    "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    private void getAllUnits() {
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + Constants.UNITS_ROUTE;
                Request request = new Request.Builder().url(url).build();
                try {
                    response = client.newCall(request).execute();
                    if (response == null) {
                        Toast.makeText(MainActivity.this,
                                "Servidor está fora do ar. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                    } else {
                        if (response.isSuccessful()) {
                            requestUnits = response.body().string();
                        } else {
                            requestUnits = new String("error");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (requestUnits != null) {
                            if (requestUnits.contains("error")) {
                                Toast.makeText(MainActivity.this,
                                        "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                            } else {
                                saveAllUnits(requestUnits);
                            }
                        } else {
                            Toast.makeText(MainActivity.this,
                                    "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    private void saveAllUnits(String requestUnits) {
        units = new ArrayList<>();
        try {
            JSONObject requestJson = new JSONObject(requestUnits);
            JSONArray unitsArray = requestJson.getJSONArray("units");
            JSONObject unit;
            for (int i = 0; i < unitsArray.length(); i++) {
                unit = new JSONObject(unitsArray.getString(i));
                Unit current_unit = new Unit(unit.getInt("id"), unit.getString("description"));
                units.add(current_unit);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getHydrantInfo(final int hydrantId, final String hydrantIcon) {
        final OkHttpClient client = new OkHttpClient();
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Carregando dados do hidrante " + hydrantId, "Aguarde alguns instantes...", false, false);

        new Thread(new Runnable() {
            @Override
            public void run() {

                String url = Constants.API + Constants.SINGLE_HYDRANT_ROUTE + hydrantId;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                builder.header("Military-Number", preferences.getString("nbm", "missing"));
                Request request = builder.build();
                try {
                    response = client.newCall(request).execute();
                    if (response == null) {
                        Toast.makeText(MainActivity.this,
                                "Servidor está fora do ar. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                    } else {
                        if (response.isSuccessful()) {
                            hydrant = response.body().string();
                        } else {
                            hydrant = new String("error");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (hydrant != null) {
                            if (hydrant.contains("error")) {
                                Toast.makeText(MainActivity.this,
                                        "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                            } else {
                                progressDialog.dismiss();
                                currentHydrant = createHydrant(hydrantId, hydrantIcon, hydrant);

                                if (isFABOpen) {
                                    closeFABMenu();
                                }

                                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment(currentHydrant, units,
                                        mUserPosition.getLocation().latitude, mUserPosition.getLocation().longitude,
                                        getHydrantIsEditable(hydrant));

                                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                            }
                        } else {
                            Toast.makeText(MainActivity.this,
                                    "Servidor está fora do ar. Tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    public Hydrant createHydrant(int hydrantId, String hydrantIcon, String hydrant) {
        try {
            JSONObject requestJson           = new JSONObject(hydrant);
            JSONObject requestHydrant        = requestJson.getJSONObject("json");
            JSONObject requestLastInspection = requestHydrant.getJSONObject("last_inspection");

            String unit            = requestHydrant.getString("unit");
            String address         = requestHydrant.getString("address");
            String reference_point = requestHydrant.getString("reference_point");
            String situation       = requestHydrant.getString("situation");
            String output          = requestHydrant.getString("output");
            String pressure        = requestHydrant.getString("pressure");
            String cap             = requestHydrant.getString("cap");
            LatLng position        = new LatLng(requestHydrant.getDouble("lat"),
                    requestHydrant.getDouble("long"));

            String lastInspectionDate = requestLastInspection.getString("date");
            String lastInspectionNReds = requestLastInspection.getString("nReds");
            String lastInspectionObs  = requestLastInspection.getString("observations");
            Inspection lastInspection  = new Inspection(lastInspectionDate, lastInspectionNReds,
                    lastInspectionObs);

            return new Hydrant(hydrantId, hydrantIcon, unit, address, reference_point, position, situation,
                    output, pressure, cap, lastInspection);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean getHydrantIsEditable(String hydrant)
    {
        try {
            JSONObject requestJson           = new JSONObject(hydrant);
            JSONObject requestHydrant        = requestJson.getJSONObject("json");

            return requestHydrant.getBoolean("isEditable");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void showFABMenu(){
        isFABOpen = true;
        exit.animate().translationY(-getResources().getDimension(R.dimen.standard_65));
        feedback.animate().translationY(-getResources().getDimension(R.dimen.standard_130));
        report.animate().translationY(-getResources().getDimension(R.dimen.standard_195));
        search.animate().translationY(-getResources().getDimension(R.dimen.standard_260));
        filter.animate().translationY(-getResources().getDimension(R.dimen.standard_325));
        plus.animate().translationY(-getResources().getDimension(R.dimen.standard_390));
    }

    private void closeFABMenu(){
        isFABOpen = false;
        exit.animate().translationY(0);
        feedback.animate().translationY(0);
        report.animate().translationY(0);
        search.animate().translationY(0);
        filter.animate().translationY(0);
        plus.animate().translationY(0);
    }

    public JSONObject createLocationJson() {
        JSONObject currentLocation = new JSONObject();

        try {
            currentLocation.put("militaryNumber", preferences.getString("nbm", "missing"));
            currentLocation.put("lat", mUserPosition.getLocation().latitude);
            currentLocation.put("long", mUserPosition.getLocation().longitude);
        } catch (JSONException e) {
            return null;
        }

        return currentLocation;
    }

    public JSONObject createSearchJson(int id) {
        JSONObject searchJson = new JSONObject();

        try {
            searchJson = createLocationJson();
            searchJson.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return searchJson;
    }

    private void getLatLongFromSearchedHydrant(JSONObject hydrantList) {
        try {
            String searchedHydrantLat = hydrantList.getString("lat");
            String searchedHydrantLong = hydrantList.getString("long");
            String searchedHydrantIconPicture = hydrantList.getString("iconPicture");

            searchedHydrant = new Hydrant(searchedHydrantIconPicture, searchHydrantId,
                    Double.valueOf(searchedHydrantLat), Double.valueOf(searchedHydrantLong));

        } catch (JSONException e) {
            searchedHydrant = null;
        }
    }

    private void centerMapOnSearchedHydrant() {
        if (searchedHydrant != null) {
            setCameraView(searchedHydrant.getPosition().latitude, searchedHydrant.getPosition().longitude);
            getHydrantInfo(searchedHydrant.getId(), searchedHydrant.getIconPicture());
        } else {
            buildAlertMessageSearchedHydrantNotFound();
        }
    }

    private void buildAlertMessageSearchedHydrantNotFound() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog);
        builder.setMessage("Não foi encontrado um hidrante com identificador " + searchHydrantId + ".")
                .setTitle(R.string.pesquisa_hidrante_title);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed(){}
}
