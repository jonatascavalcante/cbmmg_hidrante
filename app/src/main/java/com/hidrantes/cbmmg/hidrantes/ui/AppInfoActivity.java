package com.hidrantes.cbmmg.hidrantes.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.hidrantes.cbmmg.hidrantes.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static util.Constants.PERMISSIONS_REQUEST_ACCESS_WRITE_EXTERNAL_STORAGE;

public class AppInfoActivity extends AppCompatActivity {

    private boolean mWritePermissionGranted = false;
    private String fileName;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_info);
    }

    public void goBack(View v)
    {
        AppInfoActivity.this.finish();
    }

    public void viewAppGuide(View v)
    {
        fileName = "manual_app.pdf";
        openFile();
    }

    public void viewAppResolution(View v)
    {
        fileName = "memorando_app.pdf";
        openFile();
    }

    public void openFile()
    {
        if (!mWritePermissionGranted)
            checkPermissionToWrite();
        else
            copyReadAssets(fileName);
    }

    private void copyReadAssets(String fileName)
    {
        AssetManager assetManager = getAssets();

        InputStream in;
        OutputStream out;
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);

        try {
            in = assetManager.open(fileName);
            out = new FileOutputStream(file);

            copyFile(in, out);
            in.close();
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final Uri fileURI = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(fileURI, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    private void checkPermissionToWrite()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_ACCESS_WRITE_EXTERNAL_STORAGE);
        } else {
            mWritePermissionGranted = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mWritePermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mWritePermissionGranted = true;
                    copyReadAssets(fileName);
                }
            }
        }
    }
}
