package com.hidrantes.cbmmg.hidrantes.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputEditText;
import com.hidrantes.cbmmg.hidrantes.R;
import com.hidrantes.cbmmg.hidrantes.models.Hydrant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import util.Constants;
import util.KeyGenerator;
import util.MaskEditUtil;

public class AddHydrantActivity extends AppCompatActivity {

    // Required fields flags
    private boolean lat_correct = true,
            lng_correct = true,
            number_filled = false,
            valid_address = true,
            no_number_checked = false,
            is_edit = false;

    // Layout input elements
    private TextView titulo = null;
    private TextInputEditText lat = null;
    private TextInputEditText lng = null;
    private EditText street = null;
    private EditText streetNumber = null;
    private EditText neighborhood = null;
    private TextView city = null;
    private Button btnFazerVistoria = null;
    private CheckBox noNumberCk = null;

    // Hydrant object elements
    private Hydrant currentHydrant;
    private int currentHydrantId;
    private String allUnits;

    Response response = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hydrant);

        titulo = findViewById(R.id.titulo);
        lat = findViewById(R.id.input_lat);
        lng = findViewById(R.id.input_lng);
        street = findViewById(R.id.street_value);
        streetNumber = findViewById(R.id.number_value);
        noNumberCk = findViewById(R.id.no_number);
        neighborhood = findViewById(R.id.neighborhood_value);
        city = findViewById(R.id.city_value);
        btnFazerVistoria = findViewById(R.id.fazerVistoriaBtn);

        btnFazerVistoria.setEnabled(false);
        allUnits = getIntent().getStringExtra("UNITS");

        if (getIntent().getStringExtra("EDIT").equals("true")){
            is_edit = true;
            titulo.setText(R.string.editar_localizacao);
            btnFazerVistoria.setText(R.string.btn_atualizar);
            currentHydrantId = getIntent().getIntExtra("HYDRANT_ID", 0);
        }

        TextWatcher latitudeTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (coordinateHasValidLength(lat.getText().toString().length())) {
                    lat_correct = true;
                    if (lng_correct) {
                        updateAddress(convertCoordinateToDecimal(lat.getText().toString()),
                                convertCoordinateToDecimal(lng.getText().toString()), false);
                    }
                } else if (!coordinateHasValidLength(lat.getText().toString().length())) {
                    lat_correct = false;
                }

                streetNumber.setText("");
                number_filled = false;
            }

            @Override
            public void afterTextChanged(Editable s) {}
        };

        TextWatcher longitudeTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                System.out.println(after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (coordinateHasValidLength(lng.getText().toString().length())) {
                    lng_correct = true;
                    if (lat_correct) {
                        updateAddress(convertCoordinateToDecimal(lat.getText().toString()),
                                convertCoordinateToDecimal(lng.getText().toString()), false);
                    }
                } else if (!coordinateHasValidLength(lng.getText().toString().length())) {
                    lng_correct = false;
                }

                streetNumber.setText("");
                number_filled = false;
            }

            @Override
            public void afterTextChanged(Editable s) {}
        };

        TextWatcher streetNumberTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (streetNumber.getText().toString().length() > 0) {
                    number_filled = true;
                    if (no_number_checked) {
                        noNumberCk.setChecked(false);
                        no_number_checked = false;
                    }
                } else {
                    number_filled = false;
                }
                verifyRequiredFields();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        noNumberCk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    no_number_checked = true;
                    streetNumber.setText("");
                    verifyRequiredFields();
                } else {
                    no_number_checked = false;
                    verifyRequiredFields();
                }
            }
        });

        TextWatcher streetTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                verifyRequiredFields();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        TextWatcher neighborhoodTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                verifyRequiredFields();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        fillLocationData(getIntent());

        lat.addTextChangedListener(MaskEditUtil.insert(lat));
        lat.addTextChangedListener(latitudeTextWatcher);
        lng.addTextChangedListener(MaskEditUtil.insert(lng));
        lng.addTextChangedListener(longitudeTextWatcher);
        streetNumber.addTextChangedListener(streetNumberTextWatcher);
        street.addTextChangedListener(streetTextWatcher);
        neighborhood.addTextChangedListener(neighborhoodTextWatcher);

    }

    private void fillLocationData(Intent intent) {
        double latitude = intent.getDoubleExtra("LAT", 0.0);
        double longitude = intent.getDoubleExtra("LNG", 0.0);

        if (latitude != 0.0 && longitude != 0.0) {
            updateAddress(latitude, longitude, true);
        }
    }

    private String convertCoordinateToDMS(double coordinate) {
        coordinate *= -1;
        int degrees = (int) coordinate;
        double minutes = (coordinate - degrees) * 60;
        double seconds = (minutes - (int) minutes) * 60;

        return degrees + "º " + (int) minutes + "\' " +
                new DecimalFormat("##.##").format(seconds) + "\" ";
    }

    private double convertCoordinateToDecimal(String coordinate) {
        String coordinateElements[] = coordinate.split(" ");

        double degrees = Double.parseDouble(coordinateElements[0].replaceAll("\\D+",""));
        double minutes = Double.parseDouble(coordinateElements[1].replaceAll("\\D+",""));
        String[] coordinateSeconds = new String[2];

        if (coordinateElements[2].contains(",")) {
            coordinateSeconds = coordinateElements[2].replace("\"", "").split(",");
            if (coordinateSeconds[0].contains("."))
                coordinateSeconds[0] = coordinateSeconds[0].replace(".", "");
            if (coordinateSeconds[1].contains("."))
                coordinateSeconds[1] = coordinateSeconds[1].replace(".", "");
        } else {
            if (coordinateElements[2].contains("\""))
                coordinateElements[2] = coordinateElements[2].replace("\"", "");
            if (coordinateElements[2].contains("."))
                coordinateSeconds = coordinateElements[2].split("\\.");
            else {
                coordinateSeconds[0] = coordinateElements[2];
                coordinateSeconds[1] = "0";
            }
        }

        double seconds = Integer.valueOf(coordinateSeconds[0]) + (Integer.valueOf(coordinateSeconds[1]) / Math.pow(10.0, coordinateSeconds[1].length()));

        double decimalCoordinate = degrees + (minutes/60) + (seconds/3600);
        decimalCoordinate *= -1;

        return decimalCoordinate;
    }

    private void updateAddress(double latitude, double longitude, boolean isNewLoad) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            String currentAddress, currentNeigh, currentCity;

            if (addresses.get(0).getThoroughfare() != null)
                currentAddress = addresses.get(0).getThoroughfare();
            else
                currentAddress = addresses.get(0).getFeatureName();

            currentNeigh = addresses.get(0).getSubLocality();

            if (addresses.get(0).getSubAdminArea() != null)
                currentCity = addresses.get(0).getSubAdminArea();
            else
                currentCity = addresses.get(0).getLocality();

            String currentState = addresses.get(0).getAdminArea();

            street.setText(currentAddress);
            if (currentAddress != null)
                street.setEnabled(false);
            else
                street.setEnabled(true);

            neighborhood.setText(currentNeigh);
            if (currentNeigh != null)
                neighborhood.setEnabled(false);
            else
                neighborhood.setEnabled(true);

            city.setText(currentCity);

            if (isNewLoad) {
                String formattedLatitude = convertCoordinateToDMS(latitude);
                String formattedLongitude = convertCoordinateToDMS(longitude);

                lat.setText(formattedLatitude);
                lng.setText(formattedLongitude);
            }

            valid_address = verifyAddress(currentCity, currentState);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void verifyRequiredFields() {
        if (isValidAddress()) {
            btnFazerVistoria.setEnabled(true);
        } else {
            btnFazerVistoria.setEnabled(false);
        }
    }

    public boolean isValidAddress()
    {
        return lat_correct && lng_correct &&
                (number_filled || no_number_checked) && valid_address &&
                street.getText().length() > 0 && neighborhood.getText().length() > 0;
    }

    public void goBack(View v) {
        AddHydrantActivity.this.finish();
    }

    public void submitActivity(View v)
    {
        if (is_edit){
            updateLocation(v);
        } else {
            inspectNewHydrant(v);
        }
    }

    public void inspectNewHydrant(View v) {
        String address = getFormattedAddress();
        LatLng position = getFormattedPosition();
        currentHydrant = new Hydrant(Constants.DEFAULT_ICON, address, position);
        Intent intent = new Intent(AddHydrantActivity.this, InspectionActivity.class);
        intent.putExtra("HYDRANT", currentHydrant.toJson().toString());
        intent.putExtra("STREET", street.getText().toString().toUpperCase());

        if(streetNumber.getText().toString().isEmpty())
            intent.putExtra("STREET_NUMBER", "S/ Nº");
        else
            intent.putExtra("STREET_NUMBER", streetNumber.getText().toString());

        intent.putExtra("NEIGHBORHOOD", neighborhood.getText().toString().toUpperCase());
        intent.putExtra("CITY", city.getText().toString().toUpperCase());
        intent.putExtra("UNITS", allUnits);
        startActivity(intent);
    }

    public String getFormattedAddress() {

        String number = streetNumber.getText().toString().isEmpty() ? "S/ Nº" : streetNumber.getText().toString();

        return String.format("%s, %s - %s, %s",
                street.getText().toString().toUpperCase(),
                number,
                neighborhood.getText().toString().toUpperCase(),
                city.getText().toString().toUpperCase() );
    }

    public LatLng getFormattedPosition() {
        double positionLat = convertCoordinateToDecimal(lat.getText().toString());
        double positionLng = convertCoordinateToDecimal(lng.getText().toString());

        return new LatLng(positionLat, positionLng);
    }

    public void alertAddressWithInvalidState() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddHydrantActivity.this, android.R.style.Theme_Material_Light_Dialog);

        builder.setMessage(R.string.endereco_estado_invalido_dialog_msg)
                .setTitle(R.string.endereco_invalido_dialog_title);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void alertInvalidAddress() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddHydrantActivity.this, android.R.style.Theme_Material_Light_Dialog);

        builder.setMessage(R.string.endereco_invalido_dialog_msg)
                .setTitle(R.string.endereco_invalido_dialog_title);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public boolean verifyAddress(String city, String state) {
        if (city == null || state == null) {
            alertInvalidAddress();
            return false;
        } else if (!state.contains(Constants.DEFAULT_STATE)) {
            alertAddressWithInvalidState();
            return false;
        }

        return true;
    }

    public boolean coordinateHasValidLength(int coordinateLength) {
        return coordinateLength == Constants.COORDINATES_FULL_LENGTH || coordinateLength == Constants.COORDINATES_ALTERNATE_LENGTH;
    }

    public void updateLocation(View v)
    {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Atualizando localização", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + Constants.UPDATE_LOCATION;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, createAddressJson().toString());
                builder.post(body);
                Request request = builder.build();

                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddHydrantActivity.this, android.R.style.Theme_Material_Light_Dialog);
                            builder.setMessage(R.string.atualizacao_localizacao_dialog_msg)
                                    .setTitle(R.string.atualizacao_localizacao_dialog_title);
                            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(AddHydrantActivity.this, MainActivity.class);
                                    intent.putExtra("SEARCH", Integer.parseInt(String.valueOf(currentHydrantId)));
                                    startActivity(intent);
                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            Toast.makeText(AddHydrantActivity.this,
                                    "Ocorreu um erro ao realizar a vistoria. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                        }
                    }
                });
            }
        }).start();
    }

    public JSONObject createAddressJson()
    {
        JSONObject currentAddress = new JSONObject();

        try {
            currentAddress.put("hydrant_id", String.valueOf(currentHydrantId));
            currentAddress.put("street", street.getText().toString().toUpperCase());

            if (streetNumber.getText().toString().isEmpty())
                currentAddress.put("street_number", "S/ Nº");
            else
                currentAddress.put("street_number", streetNumber.getText().toString());

            currentAddress.put("neighborhood", neighborhood.getText().toString().toUpperCase());
            currentAddress.put("city", city.getText().toString().toUpperCase());

            String[] latLng = getFormattedPosition().toString().replace("lat/lng: (", "").replace(")","").split(",");
            currentAddress.put("position", String.format("%s,%s", latLng[0], latLng[1]));
        } catch (JSONException e) {
            return null;
        }

        return currentAddress;
    }

}
