package com.hidrantes.cbmmg.hidrantes.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.hidrantes.cbmmg.hidrantes.R;
import com.hidrantes.cbmmg.hidrantes.models.Unit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import util.Constants;
import util.KeyGenerator;
import util.MaskEditUtil;

public class InspectionActivity extends AppCompatActivity {

    private int currentHydrantId;
    private String currentHydrantPosition;
    Response response = null;
    private String newHydrant = null;

    // Required fields flags
    private boolean rg_status_checked = false,
            rg_output_checked = false,
            rg_pressure_checked = false,
            rg_cap_checked = false,
            nReds_correct = false;

    // Layout input elements
    private ImageView hydrantIcon = null;
    private ImageView redsValid = null;
    private ImageView redsInvalid = null;
    private TextView hydrantId = null;
    private TextView hydrantAddress = null;
    private EditText hydrantReferencePoint = null;
    private TextInputLayout nRedsTextInput = null;
    private TextInputEditText nReds = null;
    private TextView errorNReds = null;
    private RadioGroup status = null;
    private RadioGroup output = null;
    private RadioGroup pressure = null;
    private RadioGroup cap = null;
    private TextView observations = null;
    private Spinner units = null;
    private Button btn_inspect = null;

    private String redsValidator = null;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection);

        Intent intent = getIntent();
        fillInpectionFormFields(intent);

        redsValid = findViewById(R.id.reds_valid);
        redsInvalid = findViewById(R.id.reds_invalid);
        nReds = findViewById(R.id.input_n_reds);
        nRedsTextInput = findViewById(R.id.text_input_n_reds);
        errorNReds = findViewById(R.id.textinput_error);
        status = findViewById(R.id.situacao_radio_group);
        output = findViewById(R.id.vazao_radio_group);
        pressure = findViewById(R.id.pressao_radio_group);
        cap = findViewById(R.id.tampao_radio_group);
        btn_inspect = findViewById(R.id.fazerVistoriaBtn);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        btn_inspect.setEnabled(false);
        errorNReds.setPadding(0, 4, 0, 0);
        hideNRedsStatus();

        TextWatcher nRedsLengthValidatorTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (nReds.getText().toString().length() == Constants.N_REDS_FULL_LENGTH) {
                    verifyIfNRedsExists();
                } else {
                    nReds_correct = false;
                    hideNRedsStatus();
                    verifyRequiredFields();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        nReds.addTextChangedListener(MaskEditUtil.mask(nReds, Constants.N_REDS_MASK_FORMAT));
        nReds.addTextChangedListener(nRedsLengthValidatorTextWatcher);
        nReds.setText( String.valueOf(Calendar.getInstance().get(Calendar.YEAR)) );

        status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup status, int checkedId) {
                rg_status_checked = true;
                verifyRequiredFields();
            }
        });

        output.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup output, int checkedId) {
                rg_output_checked = true;
                verifyRequiredFields();
            }
        });

        pressure.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup pressure, int checkedId) {
                rg_pressure_checked = true;
                verifyRequiredFields();
            }
        });

        cap.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup cap, int checkedId) {
                rg_cap_checked = true;
                verifyRequiredFields();
            }
        });
    }

    public void fillInpectionFormFields(Intent intent) {
        JSONObject hydrant;
        JSONArray allUnitsJson;
        ArrayList<Unit> allUnits;
        hydrantIcon = findViewById(R.id.hydrant_logo);
        hydrantId = findViewById(R.id.hydrant_id);
        hydrantAddress = findViewById(R.id.hydrant_address);
        hydrantReferencePoint = findViewById(R.id.input_hydrant_reference_point);
        String unit;

        try {
            hydrant = new JSONObject(intent.getStringExtra("HYDRANT"));
            allUnitsJson = new JSONArray(intent.getStringExtra("UNITS"));
            allUnits = getAllUnitsFromJson(allUnitsJson);
            hydrantIcon.setImageResource(hydrant.getInt("icon"));
            currentHydrantId = hydrant.getInt("id");

            if(currentHydrantId == 0) {
                hydrantId.setText(getString(R.string.new_hydrant));
                String[] latLng = hydrant.getString("position").replace("lat/lng: (", "").replace(")","").split(",");
                currentHydrantPosition = String.format("%s,%s", latLng[0], latLng[1]);
            } else {
                hydrantId.setText(String.format("%s %d", getString(R.string.hydrant), hydrant.getInt("id")));
            }

            hydrantAddress.setText(hydrant.getString("address"));
            hydrantReferencePoint.setText(hydrant.getString("referencePoint"));
            unit = hydrant.getString("unit");
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Erro ao carregar hidrante.", Toast.LENGTH_SHORT).show();
            return;
        }

        fillUnitsSpinner(allUnits, unit);
    }

    public static ArrayList<Unit> getAllUnitsFromJson(JSONArray allUnitsJson) {
        ArrayList<Unit> allUnits = new ArrayList<>();
        for (int i = 0; i < allUnitsJson.length(); i++) {
            try {
                JSONObject current_unit = new JSONObject(allUnitsJson.getString(i));
                allUnits.add(new Unit(current_unit.getInt("id"),
                        current_unit.getString("description")));
            } catch (JSONException e) {
                return null;
            }
        }
        return allUnits;
    }


    public void fillUnitsSpinner(ArrayList<Unit> allUnits, String unit) {
        List<String> spinnerArray = new ArrayList<>();
        for(int i = 0; i < allUnits.size(); i++) {
            spinnerArray.add(allUnits.get(i).getDescription());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = findViewById(R.id.units_spinner);
        sItems.setAdapter(adapter);

        int spinnerPosition = adapter.getPosition(unit);
        sItems.setSelection(spinnerPosition);
    }

    public void verifyRequiredFields() {
        if(rg_status_checked && rg_output_checked && rg_pressure_checked && rg_cap_checked && nReds_correct) {
            btn_inspect.setEnabled(true);
        } else {
            btn_inspect.setEnabled(false);
        }
    }

    public void confirmInspection(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(InspectionActivity.this, android.R.style.Theme_Material_Light_Dialog);

        builder.setMessage(R.string.vistoria_confirmacao_dialog_msg)
                .setTitle(R.string.vistoria_confirmacao_dialog_title);
        builder.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(currentHydrantId == 0) {
                    inspectNewHydrant();
                } else {
                    inspect();
                }
            }
        });
        builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void inspect() {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Salvando vistoria", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + Constants.INSPECT_HYDRANT_ROUTE;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, createInspectionJson().toString());
                builder.post(body);
                Request request = builder.build();

                try {
                     response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(InspectionActivity.this, android.R.style.Theme_Material_Light_Dialog);
                            builder.setMessage(R.string.vistoria_sucesso_dialog_msg)
                                    .setTitle(R.string.vistoria_sucesso_dialog_title);
                            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent it = new Intent(InspectionActivity.this, MainActivity.class);
                                    it.putExtra("SEARCH", Integer.parseInt(String.valueOf(currentHydrantId)));
                                    startActivity(it);
                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            Toast.makeText(InspectionActivity.this,
                                    "Ocorreu um erro ao realizar a vistoria. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                        }
                    }
                });
            }
        }).start();
    }

    public void inspectNewHydrant() {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Cadastrando novo hidrante", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + Constants.ADD_HYDRANT_ROUTE;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, createInspectionJson().toString());
                builder.post(body);
                Request request = builder.build();

                try {
                    response = client.newCall(request).execute();
                    if (response.isSuccessful()) {
                        newHydrant = response.body().string();
                    } else {
                        newHydrant = new String("error");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        if (newHydrant != null) {
                            String newHydrantId = "";
                            progressDialog.dismiss();
                            try {
                                JSONObject newHydrantJson = new JSONObject(newHydrant);
                                newHydrantId = newHydrantJson.getString("hydrantId");
                                currentHydrantId = Integer.parseInt(newHydrantId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder(InspectionActivity.this, android.R.style.Theme_Material_Light_Dialog);
                            builder.setMessage("Novo hidrante cadastrado com sucesso!\n\nID do novo hidrante: " + newHydrantId)
                                    .setTitle(R.string.hidrante_novo_sucesso_dialog_title);
                            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent it = new Intent(InspectionActivity.this, MainActivity.class);
                                    it.putExtra("SEARCH", Integer.parseInt(String.valueOf(currentHydrantId)));
                                    startActivity(it);
                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            Toast.makeText(InspectionActivity.this,
                                    "Ocorreu um erro ao cadastrar o novo hidrante. Tente novamente mais tarde", Toast.LENGTH_SHORT);
                        }
                    }
                });
            }
        }).start();
    }

    public void goBack(View v) {
        InspectionActivity.this.finish();
    }

    public JSONObject createInspectionJson() {

        JSONObject currentInspection = new JSONObject();
        observations = findViewById(R.id.input_observacoes);
        units = findViewById(R.id.units_spinner);

        RadioButton statusSelected = findViewById(status.getCheckedRadioButtonId());
        RadioButton outputSelected = findViewById(output.getCheckedRadioButtonId());
        RadioButton pressureSelected = findViewById(pressure.getCheckedRadioButtonId());
        RadioButton capSelected = findViewById(cap.getCheckedRadioButtonId());

        try {
            currentInspection.put("militaryNumber", preferences.getString("nbm", "missing"));
            currentInspection.put("referencePoint", hydrantReferencePoint.getText().toString());
            currentInspection.put("nReds", nReds.getText().toString());
            currentInspection.put("status", statusSelected.getText());
            currentInspection.put("output", outputSelected.getText());
            currentInspection.put("pressure", pressureSelected.getText());
            currentInspection.put("cap", capSelected.getText());
            currentInspection.put("observations", observations.getText().toString());
            currentInspection.put("unitDescription", units.getSelectedItem().toString());

            if (currentHydrantId != 0) {
                currentInspection.put("hydrantId", currentHydrantId);
            } else {
                currentInspection.put("street", getIntent().getStringExtra("STREET"));

                if (getIntent().getStringExtra("STREET_NUMBER").equals("S/ Nº"))
                    currentInspection.put("street_number", "0");
                else
                    currentInspection.put("street_number", getIntent().getStringExtra("STREET_NUMBER"));

                currentInspection.put("neighborhood", getIntent().getStringExtra("NEIGHBORHOOD"));
                currentInspection.put("city", getIntent().getStringExtra("CITY"));
                currentInspection.put("position", currentHydrantPosition);
            }

        } catch (JSONException e) {
            return null;
        }

        return currentInspection;
    }


    public void verifyIfNRedsExists() {
        final ProgressDialog progressDialog = ProgressDialog.show(this,
                "Verificando Nº do REDS", "Aguarde alguns instantes...", false, false);
        final OkHttpClient client = new OkHttpClient();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = Constants.API + Constants.REDS_VERIFIER_ROUTE;
                Request.Builder builder = new Request.Builder();
                builder.url(url);
                builder.header("Api-Token", KeyGenerator.generate());
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, createNRedsJson().toString());
                builder.post(body);
                Request request = builder.build();

                try {
                    response = client.newCall(request).execute();

                    if (response.isSuccessful())
                        redsValidator = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        if (redsValidator != null)
                            validateRedsNumber(redsValidator);
                    }
                });
            }
        }).start();
    }

    public JSONObject createNRedsJson() {
        JSONObject currentNReds = new JSONObject();

        try {
            currentNReds.put("militaryNumber", preferences.getString("nbm", "missing"));
            currentNReds.put("nReds", nReds.getText().toString());
        } catch (JSONException e) {
            return null;
        }

        return currentNReds;
    }

    public void validateRedsNumber(String redsValidator) {
        try {
            JSONObject jsonResponse = new JSONObject(redsValidator);

            if (!jsonResponse.getBoolean("exists")) {
                nReds_correct = true;
                redsValid.setVisibility(View.VISIBLE);
                redsInvalid.setVisibility(View.GONE);
                nRedsTextInput.setError(null);
            } else {
                nReds_correct = false;
                redsValid.setVisibility(View.GONE);
                redsInvalid.setVisibility(View.VISIBLE);
                nRedsTextInput.setError("Nº REDS já utilizado");
            }

            verifyRequiredFields();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void hideNRedsStatus() {
        redsValid.setVisibility(View.GONE);
        redsInvalid.setVisibility(View.GONE);
        nRedsTextInput.setError(null);
    }
}
