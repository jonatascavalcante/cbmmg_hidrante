package util;

public class Constants {

    public static final int ERROR_DIALOG_REQUEST = 9001;
    public static final int PERMISSIONS_REQUEST_ENABLE_GPS = 9002;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9003;
    public static final int PERMISSIONS_REQUEST_ACCESS_WRITE_EXTERNAL_STORAGE = 1024;
    public static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    public static final int N_REDS_FULL_LENGTH = 18;
    public static final int COORDINATES_FULL_LENGTH = 14;
    public static final int COORDINATES_ALTERNATE_LENGTH = 13;

    public static final String API = "http://hml.api.hidrante.bombeiros.mg.gov.br/";
    public static final String FILTERED_HYDRANTS_ROUTE = "filteredHydrants";
    public static final String EXPORTED_HYDRANTS_ROUTE = "exportedHydrants";
    public static final String SEARCHED_HYDRANTS_ROUTE = "searchHydrant";
    public static final String DELIMITED_HYDRANTS_ROUTE = "delimitedHydrants";
    public static final String UNITS_ROUTE = "units";
    public static final String SINGLE_HYDRANT_ROUTE = "hydrants/";
    public static final String DELETE_HYDRANT_ROUTE = "delete";
    public static final String ADD_HYDRANT_ROUTE = "add";
    public static final String INSPECT_HYDRANT_ROUTE = "inspect";
    public static final String UPDATE_LOCATION = "updateAddress";
    public static final String REDS_VERIFIER_ROUTE = "nReds";

    public static final String DEFAULT_ICON = "red_hydrant";
    public static final String DEFAULT_STATE = "Minas Gerais";
    public static final String GOOGLE_MAPS_URL_START = "http://maps.google.com/maps?saddr=";
    public static final String GOOGLE_MAPS_URL_MIDDLE = "&daddr=";
    public static final String N_REDS_MASK_FORMAT = "####-#########-###";
    public static final String DATE_SIMPLE_FORMAT = "##/##/##";
    public static final String COORDINATE_ALTERNATE_FORMAT = "##º ##' ##.#\"";
    public static final String COORDINATE_DEFAULT_FORMAT = "##º ##' ##.##\"";
    public static final String DEFAULT_UNIT = "1 BBM";
    public static final String ALL_UNITS = "TODAS UNIDADES";

    public static final String SECRET = "hydrant";
}
