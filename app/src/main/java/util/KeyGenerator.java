package util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

public abstract class KeyGenerator {

    public static String generate() {
        String key = String.format("%s%s", getYearAndDay(), Constants.SECRET);
        String hash;

        try {
            MessageDigest message = MessageDigest.getInstance("MD5");
            message.reset();
            message.update(key.getBytes());
            hash = new BigInteger(1, message.digest()).toString(16);
            while(hash.length() < 32 ){
                hash = "0" + hash;
            }
            return hash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getYearAndDay() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        if (day > 9) {
            return String.format("%d-%d", year, day);
        } else {
            return String.format("%d-0%d", year, day);
        }
    }
}
