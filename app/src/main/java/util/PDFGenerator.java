package util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;

import com.hidrantes.cbmmg.hidrantes.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PDFGenerator {

    private Context context;
    public static final int MAX_OF_CHARACTERS_ADDRESS = 55;
    public static final int MAX_OF_CHARACTERS_REFERENCE_POINT = 52;
    public static final int MAX_OF_CHARACTERS_OBSERVATIONS = 50;

    public PDFGenerator(Context current){
        this.context = current;
    }

    public PdfDocument createPDF(String hydrants, JSONObject filters)
    {
        PdfDocument document = new PdfDocument();
        JSONArray hydrantsArray = new JSONArray();

        try {
            JSONObject hydrantList = new JSONObject(hydrants);
            hydrantsArray = hydrantList.getJSONArray("json");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int nPages = (int) (1 + Math.ceil((hydrantsArray.length() - 4)/5.0));

        for (int i = 1; i < nPages + 1; i++) {
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(2099, 2970, i).create();
            PdfDocument.Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();

            if (i == 1) {
                createDocumentHeader(canvas);
                try {
                    createDocumentInfoHeader(canvas, filters, hydrantsArray.length());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (hydrantsArray.length() == 0) {
                    createNoResultsMessage(canvas);
                    document.finishPage(page);
                    break;
                }
                createFirstPageHydrantColumn(canvas, hydrantsArray);
            } else {
                createPageHydrantColumn(canvas, hydrantsArray, i);
            }

            Paint pageNumberCounter = new Paint();
            pageNumberCounter.setColor(Color.BLACK);
            pageNumberCounter.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
            pageNumberCounter.setTextSize(40);
            canvas.drawText(String.format("Página %d de %d", i, nPages), 900, 2900, pageNumberCounter);

            document.finishPage(page);
        }

        return document;
    }

    private void createDocumentHeader(Canvas canvas)
    {
        Paint paint = new Paint();

        Paint headerRectangleBorder = new Paint();
        headerRectangleBorder.setColor(Color.BLACK);
        canvas.drawRect(202, 139, 1897, 345, headerRectangleBorder);

        Paint headerRectangleBody = new Paint();
        headerRectangleBody.setColor(Color.WHITE);
        canvas.drawRect(207, 144, 1892, 340, headerRectangleBody);

        Paint title = new Paint();
        title.setColor(Color.BLACK);
        title.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        title.setTextSize(45);
        canvas.drawText("CBMMG - LEVANTAMENTO DE HIDRANTES", 697, 260, title);

        Bitmap logo = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
        Bitmap scaledLogo = Bitmap.createScaledBitmap(logo, 295, 151, false);
        canvas.drawBitmap(scaledLogo, 228, 167, paint);
    }

    private void createDocumentInfoHeader(Canvas canvas, JSONObject filters, int hydrantsAmount)
    {
        String date = "", status = "", unit = "", inspectDate = "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YY");

        try {
            date = formatter.format(new Date());
            status = filters.getString("status");

            if (filters.has("dateFrom") && filters.has("dateTo")) {
                inspectDate = String.format("%s à %s", filters.get("dateFrom"), filters.getString("dateTo"));
            } else if (filters.has("dateFrom") && !filters.has("dateTo")) {
                inspectDate = String.format("A partir de %s", filters.get("dateFrom"));
            } else if (!filters.has("dateFrom") && filters.has("dateTo")) {
                inspectDate = String.format("Até %s", filters.get("dateTo"));
            } else {
                inspectDate = "Não especificada";
            }

            unit = filters.getString("unit");
        } catch (JSONException e) {
            if (unit.equals("")) unit = "Todas unidades";
        }

        Paint dateTag = new Paint();
        dateTag.setColor(Color.BLACK);
        dateTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        dateTag.setTextSize(40);
        canvas.drawText("Data:", 202, 430, dateTag);

        Paint dateValue = new Paint();
        dateValue.setColor(Color.BLACK);
        dateValue.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        dateValue.setTextSize(40);
        canvas.drawText(date, 312, 430, dateValue);

        Paint filterTag = new Paint();
        filterTag.setColor(Color.BLACK);
        filterTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        filterTag.setTextSize(40);
        canvas.drawText("Status:", 202, 480, filterTag);

        Paint filterValue = new Paint();
        filterValue.setColor(Color.BLACK);
        filterValue.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        filterValue.setTextSize(40);
        canvas.drawText(status, 342, 480, filterValue);

        Paint unitTag = new Paint();
        unitTag.setColor(Color.BLACK);
        unitTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        unitTag.setTextSize(40);
        canvas.drawText("Unidade:", 202, 530, unitTag);

        Paint unitValue = new Paint();
        unitValue.setColor(Color.BLACK);
        unitValue.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        unitValue.setTextSize(40);
        canvas.drawText(unit, 372, 530, unitValue);

        Paint inspectDateTag = new Paint();
        inspectDateTag.setColor(Color.BLACK);
        inspectDateTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        inspectDateTag.setTextSize(40);
        canvas.drawText("Data de vistoria:", 202, 580, inspectDateTag);

        Paint inspectDateValue = new Paint();
        inspectDateValue.setColor(Color.BLACK);
        inspectDateValue.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        inspectDateValue.setTextSize(40);
        canvas.drawText(inspectDate, 502, 580, inspectDateValue);

        Paint hydrantsAmountTag = new Paint();
        hydrantsAmountTag.setColor(Color.BLACK);
        hydrantsAmountTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        hydrantsAmountTag.setTextSize(40);
        canvas.drawText("Quantidade de hidrantes:", 202, 630, hydrantsAmountTag);

        Paint hydrantsAmountText = new Paint();
        hydrantsAmountText.setColor(Color.BLACK);
        hydrantsAmountText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        hydrantsAmountText.setTextSize(40);
        canvas.drawText((String.valueOf(hydrantsAmount)), 662, 630, inspectDateValue);
    }

    private void createNoResultsMessage(Canvas canvas)
    {
        Paint title = new Paint();
        title.setColor(Color.BLACK);
        title.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        title.setTextSize(40);
        canvas.drawText("Nenhum hidrante encontrado com os filtros especificados", 550, 750, title);
    }

    private void createFirstPageHydrantColumn(Canvas canvas, JSONArray hydrantsArray)
    {
        int hydrantsAmount = Math.min(hydrantsArray.length(), 4);

        for (int i = 0; i < hydrantsAmount; i++) {
            Paint tableRectangleBorder = new Paint();
            tableRectangleBorder.setColor(Color.BLACK);
            canvas.drawRect(202, (703 + 522*i), 1897, (1225 + 522*i), tableRectangleBorder);

            Paint tableHeaderRectangle = new Paint();
            tableHeaderRectangle.setColor(Color.WHITE);
            canvas.drawRect(206, (707 + 522*i), 1893, (805 + 522*i), tableHeaderRectangle);

            Paint tableLeftIconRectangle = new Paint();
            tableLeftIconRectangle.setColor(Color.WHITE);
            canvas.drawRect(206, (809 + 522*i), 496, (1223 + 522*i), tableLeftIconRectangle);

            Paint tableMainRectangle = new Paint();
            tableMainRectangle.setColor(Color.WHITE);
            canvas.drawRect(500, (809 + 522*i), 1893, (1223 + 522*i), tableMainRectangle);
        }

        fillHydrantsList(canvas, hydrantsArray, hydrantsAmount, 1);
    }

    private void createPageHydrantColumn(Canvas canvas, JSONArray hydrantsArray, int pageNumber)
    {
        int hydrantsAmount = Math.min(hydrantsArray.length() - (4 + 5 * (pageNumber - 2)), 5);

        for (int i = 0; i < hydrantsAmount; i++) {
            Paint tableRectangleBorder = new Paint();
            tableRectangleBorder.setColor(Color.BLACK);
            canvas.drawRect(202, (143 + 522*i), 1897, (665 + 522*i), tableRectangleBorder);

            Paint tableHeaderRectangle = new Paint();
            tableHeaderRectangle.setColor(Color.WHITE);
            canvas.drawRect(206, (147 + 522*i), 1893, (245 + 522*i), tableHeaderRectangle);

            Paint tableLeftIconRectangle = new Paint();
            tableLeftIconRectangle.setColor(Color.WHITE);
            canvas.drawRect(206, (249 + 522*i), 496, (663 + 522*i), tableLeftIconRectangle);

            Paint tableMainRectangle = new Paint();
            tableMainRectangle.setColor(Color.WHITE);
            canvas.drawRect(500, (249 + 522*i), 1893, (663 + 522*i), tableMainRectangle);
        }

        fillHydrantsList(canvas, hydrantsArray, hydrantsAmount, pageNumber);
    }

    private void fillHydrantsList(Canvas canvas, JSONArray hydrantsArray, int hydrantsAmount, int pageNumber)
    {
        int initialIndex, finalIndex;

        if (pageNumber == 1) {
            initialIndex = 0;
            finalIndex = hydrantsAmount;
        } else {
            initialIndex = 4 + (pageNumber - 2) * 5;
            finalIndex = initialIndex + hydrantsAmount;
        }

        for (int index = initialIndex, tableIndex = 0; index < finalIndex; index++, tableIndex++) {
            try {
                JSONObject currentHydrant = hydrantsArray.getJSONObject(index);

                Paint hydrantId = new Paint();
                hydrantId.setColor(Color.BLACK);
                hydrantId.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantId.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(String.format("Hidrante %s", currentHydrant.getString("id")), 900, (770 + 522 * tableIndex), hydrantId);
                else
                    canvas.drawText(String.format("Hidrante %s", currentHydrant.getString("id")), 900, (210 + 522 * tableIndex), hydrantId);

                Paint hydrantAddressTag = new Paint();
                hydrantAddressTag.setColor(Color.BLACK);
                hydrantAddressTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantAddressTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("ENDEREÇO: ", 510, (875 + 522 * tableIndex), hydrantAddressTag);
                else
                    canvas.drawText("ENDEREÇO: ", 510, (315 + 522 * tableIndex), hydrantAddressTag);

                Paint hydrantAddress = new Paint();
                hydrantAddress.setColor(Color.BLACK);
                hydrantAddress.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantAddress.setTextSize(40);
                String address = currentHydrant.getString("address");
                if (address.length() >= MAX_OF_CHARACTERS_ADDRESS)
                    address = address.substring(0, MAX_OF_CHARACTERS_ADDRESS);
                if (pageNumber == 1)
                    canvas.drawText(address, 736, (875 + 522 * tableIndex), hydrantAddress);
                else
                    canvas.drawText(address, 736, (315 + 522 * tableIndex), hydrantAddress);

                Paint hydrantReferencePointTag = new Paint();
                hydrantReferencePointTag.setColor(Color.BLACK);
                hydrantReferencePointTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantReferencePointTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("REFERÊNCIA: ", 510, (925 + 522 * tableIndex), hydrantReferencePointTag);
                else
                    canvas.drawText("REFERÊNCIA: ", 510, (365 + 522 * tableIndex), hydrantReferencePointTag);

                Paint hydrantReferencePoint = new Paint();
                hydrantReferencePoint.setColor(Color.BLACK);
                hydrantReferencePoint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantReferencePoint.setTextSize(40);
                String referencePoint = currentHydrant.getString("reference_point");
                if (referencePoint.length() >= MAX_OF_CHARACTERS_REFERENCE_POINT)
                    referencePoint = referencePoint.substring(0, MAX_OF_CHARACTERS_REFERENCE_POINT);
                if (pageNumber == 1)
                    canvas.drawText(referencePoint, 770, (925 + 522 * tableIndex), hydrantReferencePoint);
                else
                    canvas.drawText(referencePoint, 770, (365 + 522 * tableIndex), hydrantReferencePoint);

                Paint hydrantLastInspectionTag = new Paint();
                hydrantLastInspectionTag.setColor(Color.BLACK);
                hydrantLastInspectionTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantLastInspectionTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("ÚLTIMA VISTORIA: ", 510, (975 + 522 * tableIndex), hydrantLastInspectionTag);
                else
                    canvas.drawText("ÚLTIMA VISTORIA: ", 510, (415 + 522 * tableIndex), hydrantLastInspectionTag);

                Paint hydrantLastInspection = new Paint();
                hydrantLastInspection.setColor(Color.BLACK);
                hydrantLastInspection.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantLastInspection.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("last_inspection_date"), 860, (975 + 522 * tableIndex), hydrantLastInspection);
                else
                    canvas.drawText(currentHydrant.getString("last_inspection_date"), 860, (415 + 522 * tableIndex), hydrantLastInspection);

                Paint hydrantLastInspectionRedsTag = new Paint();
                hydrantLastInspectionRedsTag.setColor(Color.BLACK);
                hydrantLastInspectionRedsTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantLastInspectionRedsTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("Nº REDS: ", 1250, (975 + 522 * tableIndex), hydrantLastInspectionRedsTag);
                else
                    canvas.drawText("Nº REDS: ", 1250, (415 + 522 * tableIndex), hydrantLastInspectionRedsTag);

                Paint hydrantLastInspectionReds = new Paint();
                hydrantLastInspectionReds.setColor(Color.BLACK);
                hydrantLastInspectionReds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantLastInspectionReds.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("last_inspection_n_reds"), 1430, (975 + 522 * tableIndex), hydrantLastInspectionReds);
                else
                    canvas.drawText(currentHydrant.getString("last_inspection_n_reds"), 1430, (415 + 522 * tableIndex), hydrantLastInspectionReds);

                Paint hydrantStatusTag = new Paint();
                hydrantStatusTag.setColor(Color.BLACK);
                hydrantStatusTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantStatusTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("SITUAÇÃO: ", 510, (1025 + 522 * tableIndex), hydrantStatusTag);
                else
                    canvas.drawText("SITUAÇÃO: ", 510, (465 + 522 * tableIndex), hydrantStatusTag);

                Paint hydrantStatus = new Paint();
                hydrantStatus.setColor(Color.BLACK);
                hydrantStatus.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantStatus.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("situation"), 730, (1025 + 522 * tableIndex), hydrantStatus);
                else
                    canvas.drawText(currentHydrant.getString("situation"), 730, (465 + 522 * tableIndex), hydrantStatus);

                Paint hydrantOutputTag = new Paint();
                hydrantOutputTag.setColor(Color.BLACK);
                hydrantOutputTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantOutputTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("VAZÃO: ", 1250, (1025 + 522 * tableIndex), hydrantOutputTag);
                else
                    canvas.drawText("VAZÃO: ", 1250, (465 + 522 * tableIndex), hydrantOutputTag);

                Paint hydrantOutput = new Paint();
                hydrantOutput.setColor(Color.BLACK);
                hydrantOutput.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantOutput.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("output"), 1400, (1025 + 522 * tableIndex), hydrantOutput);
                else
                    canvas.drawText(currentHydrant.getString("output"), 1400, (465 + 522 * tableIndex), hydrantOutput);

                Paint hydrantPressureTag = new Paint();
                hydrantPressureTag.setColor(Color.BLACK);
                hydrantPressureTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantPressureTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("PRESSÃO: ", 510, (1075 + 522 * tableIndex), hydrantPressureTag);
                else
                    canvas.drawText("PRESSÃO: ", 510, (515 + 522 * tableIndex), hydrantPressureTag);

                Paint hydrantPressure = new Paint();
                hydrantPressure.setColor(Color.BLACK);
                hydrantPressure.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantPressure.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("pressure"), 715, (1075 + 522 * tableIndex), hydrantPressure);
                else
                    canvas.drawText(currentHydrant.getString("pressure"), 715, (515 + 522 * tableIndex), hydrantPressure);

                Paint hydrantCapTag = new Paint();
                hydrantCapTag.setColor(Color.BLACK);
                hydrantCapTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantCapTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("TAMPÃO: ", 1250, (1075 + 522 * tableIndex), hydrantCapTag);
                else
                    canvas.drawText("TAMPÃO: ", 1250, (515 + 522 * tableIndex), hydrantCapTag);

                Paint hydrantCap = new Paint();
                hydrantCap.setColor(Color.BLACK);
                hydrantCap.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantCap.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("cap"), 1435, (1075 + 522 * tableIndex), hydrantCap);
                else
                    canvas.drawText(currentHydrant.getString("cap"), 1435, (515 + 522 * tableIndex), hydrantCap);

                Paint hydrantObservationsTag = new Paint();
                hydrantObservationsTag.setColor(Color.BLACK);
                hydrantObservationsTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantObservationsTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("OBSERVAÇÕES: ", 510, (1125 + 522 * tableIndex), hydrantObservationsTag);
                else
                    canvas.drawText("OBSERVAÇÕES: ", 510, (565 + 522 * tableIndex), hydrantObservationsTag);

                Paint hydrantObservations = new Paint();
                hydrantObservations.setColor(Color.BLACK);
                hydrantObservations.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantObservations.setTextSize(40);
                String observations = currentHydrant.getString("observations");
                if (observations.length() >= MAX_OF_CHARACTERS_OBSERVATIONS)
                    observations = observations.substring(0, MAX_OF_CHARACTERS_OBSERVATIONS);
                if (pageNumber == 1)
                    canvas.drawText(observations, 815, (1125 + 522 * tableIndex), hydrantObservations);
                else
                    canvas.drawText(observations, 815, (565 + 522 * tableIndex), hydrantObservations);

                Paint hydrantLatLngTag = new Paint();
                hydrantLatLngTag.setColor(Color.BLACK);
                hydrantLatLngTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantLatLngTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("LAT/LNG: ", 510, (1175 + 522 * tableIndex), hydrantLatLngTag);
                else
                    canvas.drawText("LAT/LNG: ", 510, (615 + 522 * tableIndex), hydrantLatLngTag);

                Paint hydrantLatLng = new Paint();
                hydrantLatLng.setColor(Color.BLACK);
                hydrantLatLng.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantLatLng.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("lat_long"), 700, (1175 + 522 * tableIndex), hydrantLatLng);
                else
                    canvas.drawText(currentHydrant.getString("lat_long"), 700, (615 + 522 * tableIndex), hydrantLatLng);

                Paint hydrantUnitTag = new Paint();
                hydrantUnitTag.setColor(Color.BLACK);
                hydrantUnitTag.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                hydrantUnitTag.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText("UNIDADE: ", 1250, (1175 + 522 * tableIndex), hydrantUnitTag);
                else
                    canvas.drawText("UNIDADE: ", 1250, (615 + 522 * tableIndex), hydrantUnitTag);

                Paint hydrantUnit = new Paint();
                hydrantUnit.setColor(Color.BLACK);
                hydrantUnit.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                hydrantUnit.setTextSize(40);
                if (pageNumber == 1)
                    canvas.drawText(currentHydrant.getString("unit"), 1450, (1175 + 522 * tableIndex), hydrantUnit);
                else
                    canvas.drawText(currentHydrant.getString("unit"), 1450, (615 + 522 * tableIndex), hydrantUnit);

                Bitmap hydrantIcon, scaledHydrantIcon;
                Paint paint = new Paint();

                switch (currentHydrant.getString("icon")) {
                    case "yellow_hydrant":
                        hydrantIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.yellow_hydrant);
                        scaledHydrantIcon = Bitmap.createScaledBitmap(hydrantIcon, 267, 330, false);
                        if (pageNumber == 1)
                            canvas.drawBitmap(scaledHydrantIcon, 215, (850 + 522 * tableIndex), paint);
                        else
                            canvas.drawBitmap(scaledHydrantIcon, 215, (290 + 522 * tableIndex), paint);
                        break;
                    case "black_hydrant":
                        hydrantIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.dark_hydrant);
                        scaledHydrantIcon = Bitmap.createScaledBitmap(hydrantIcon, 267, 330, false);
                        if (pageNumber == 1)
                            canvas.drawBitmap(scaledHydrantIcon, 215, (850 + 522 * tableIndex), paint);
                        else
                            canvas.drawBitmap(scaledHydrantIcon, 215, (290 + 522 * tableIndex), paint);
                        break;
                    case "block_hydrant":
                        hydrantIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.forbidden_hydrant);
                        scaledHydrantIcon = Bitmap.createScaledBitmap(hydrantIcon, 341, 341, false);
                        if (pageNumber == 1)
                            canvas.drawBitmap(scaledHydrantIcon, 190, (850 + 522 * tableIndex), paint);
                        else
                            canvas.drawBitmap(scaledHydrantIcon, 190, (290 + 522 * tableIndex), paint);
                        break;
                    default:
                        hydrantIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.red_hydrant);
                        scaledHydrantIcon = Bitmap.createScaledBitmap(hydrantIcon, 267, 330, false);
                        if (pageNumber == 1)
                            canvas.drawBitmap(scaledHydrantIcon, 215, (850 + 522 * tableIndex), paint);
                        else
                            canvas.drawBitmap(scaledHydrantIcon, 215, (290 + 522 * tableIndex), paint);
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
